
const { where } = require('sequelize')
const {User} = require('../models')

exports.register = async(req,res,next)=>{
    try {
        User.create({
            email: req.body.email,
            password: req.body.password,
            role: req.body.role
        }).then(item => {
            console.log(item)
            return res.json(item)
        })
        // return res.status(200).json(response)
    } catch (error) {
        console.log(error)
        return req.json(`server error: ${error}`)
    }
}
exports.findAll = async(req,res,next)=>{
    try {
        User.findAll()
        .then(items => {
            console.log(items)
            return res.json(items)
        })
    } catch (error) {
        console.log(error)
        return req.json(`server error: ${error}`)
    }
}
exports.login = async(req,res,next)=>{
    try {
        const login = await User.findOne({where : {email:req.body.email}})
        if (login && login.password === req.body.password){
            return res.json(`login success, welcome ${login.email}`)
        } else {
            return res.json(`login gagal, email and password not match`)
        }
    } catch (error) {
        console.log(error)
        return req.json(`server error: ${error}`)
    }
}
exports.RemoveAccount = async(req,res,next)=>{
    try {
        const foundUser = await User.findOne({where : {email:req.body.email}})
        if (foundUser.password == req.body.password){
            User.destroy({
                where : {email : req.body.email}
            })
            .then(()=>{
                return res.json(`your account removed, goodbay ${req.body.email}`)
            })
            .catch(err =>{
                return res.json(`your account failed,`)
            })
            return res.json(`your account removed, goodbay ${req.body.email}`)
        } else {
            return res.json(`remove account failed, email and password not match`)
        }

    } catch (error) {
        console.log(error)
        return res.json(`server error: ${error}`)        
    }
}








// pindahan dari index.js
// app.get('/register', (req, res) => { //api register
//     const input_email = req.body.email //karena panjang, kita bikin jadi variable// email kita ambil value dari body
//     const input_password = req.body.password // membuat variable untuk password
//     const input_role = req.body.role //

//     User.create({//ini untuk input data base 
//         email: input_email,
//         password: input_password,
//         role: input_role
//     })
//         .then(User => {
//             console.log(User)
//             res.json(User);
//         })
//         .catch(() => {
//             res.send("gagal hadehh")
//         })
//     // res adalah bagian paling bawah dari api. jadi hanya bisa bikiun 1 res, untuk menampilkan body
// });

// app.get('/allUser', (req,res) => {
//     User.findAll()
//     .then(User => {
//         console.log(User)
//         res.json(User);
//     })
//     .catch(() => {
//         res.send("gagal hadehh haduh findaall")
//     })
// })