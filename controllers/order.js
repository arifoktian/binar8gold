const {User,items,order} = require('../models')

exports.order = async(req,res,next)=>{
    try {
        order.create({
            user_id: req.body.user_id,
            item_id: req.body.item_id,
            item: req.body.item,
            address: req.body.address,
            status: req.body.status,
            total: req.body.total
        }).then(item => {
            console.log(item)
            return res.json(item)
        })
    } catch (error) {
        console.log(error)
        return res.json(error)
    }
}
exports.updateOrder = async(req,res,next)=>{
    try {
        order.update({
            user_id: req.body.user_id,
            item_id: req.body.item_id,
            item: req.body.item,
            address: req.body.address,
            status: req.body.status,
            total: req.body.total
            },
            {
                where : {id : req.params.id}
            })
            .then(item => {
                console.log(item)
                return res.status(200).json(item)
        })
    } catch (error) {
        console.log(error)
        return res.status(500).json(error)
    }
}