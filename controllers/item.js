const {User,items} = require('../models')

exports.createOne = async(req,res,next)=>{
    try {
        items.create({
            id_product: req.body.id_product,
            nama_product: req.body.nama_product,
            qty: req.body.qty,
            harga: req.body.harga
        }).then(item => {
            console.log(item)
            return res.status(200).json(item)
        })
    } catch (error) {
        console.log(error)
        return res.status(500).json(error)
    }
}

exports.findAll = async(req,res,next)=>{
    try {
        items.findAll({
            include: [{
                model: User
            }]
        })
        .then(item => {
            console.log(item)
            return res.status(200).json(item)
        })
    } catch (error) {
        console.log(error)
        return res.status(500).json(error)
    }
}
// belum ada yang mine

exports.updateOne = async(req,res,next)=>{
    try {
        items.update({
                id_product: req.body.id_product,
                nama_product: req.body.nama_product,
                qty: req.body.qty,
                harga: req.body.harga
            },
            {
                where : {id : req.params.id}
            })
            .then(item => {
                console.log(item)
                return res.status(200).json(item)
        })
    } catch (error) {
        console.log(error)
        return res.status(500).json(error)
    }
}
// berhasil update product tapi notif di postman cuek

