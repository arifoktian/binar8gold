'use strict';
const {
  Model
} = require('sequelize');
module.exports = (sequelize, DataTypes) => {
  class order extends Model {
    /**
     * Helper method for defining associations.
     * This method is not a part of Sequelize lifecycle.
     * The `models/index` file will call this method automatically.
     */
    static associate(models) {
      // define association here
      this.belongsTo(models.User, {foreignKey: 'user_id'})
      this.belongsTo(models.items, {foreignKey: 'item_id'})
    }
  }
  order.init({
    user_id: DataTypes.INTEGER,
    item_id: DataTypes.INTEGER,
    address: DataTypes.STRING,
    status: DataTypes.STRING,
    total: DataTypes.STRING
  }, {
    sequelize,
    modelName: 'order',
  });
  return order;
};