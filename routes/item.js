const ctrl = require('../controllers/item')
const router = require('express').Router()

router
    .post('/create',ctrl.createOne)
    .get('/all',ctrl.findAll) // belum terasosiate ke user
    .put('/edit/:id',ctrl.updateOne)
module.exports = router
