const ctrl = require('../controllers/order')
const router = require('express').Router()

router
    .post('/order', ctrl.order)
    .put('/edit/:id',ctrl.updateOrder)
    
module.exports = router
