const ctrl = require('../controllers/user')
const router = require('express').Router()

router
    .post('/register', ctrl.register)
    .post('/login', ctrl.login)
    .get('/all',ctrl.findAll)
    .delete('/delete',ctrl.RemoveAccount)
module.exports = router
