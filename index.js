const express = require('express')
const app = express()
const { sequelize } = require('./models')
// langkah 1, membuat api register
app.use(express.urlencoded({ extended: true }));//midle ware
app.use(express.json())//ini adlaah midle ware yang kita gunakan, json dan urlunc

app.use('/user', require('./routes/user'))
app.use('/item', require('./routes/item'))
app.use('/order', require('./routes/order'))

app.get("/",(req,res)=>{
    console.log("test root api runung")
    return res.send("Test root api running")
})

app.listen(4000, () => {

    console.log("Server Running on localhost:4000")

})

//bikin api, > buat input data nomor / email dan password
